class CreateJuniors < ActiveRecord::Migration
  def change
    create_table :juniors do |t|
      t.string :name
      t.boolean :available

      t.timestamps
    end
  end
end
