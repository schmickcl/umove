require 'test_helper'

class JuniorsControllerTest < ActionController::TestCase
  setup do
    @junior = juniors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:juniors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create junior" do
    assert_difference('Junior.count') do
      post :create, junior: { available: @junior.available, name: @junior.name }
    end

    assert_redirected_to junior_path(assigns(:junior))
  end

  test "should show junior" do
    get :show, id: @junior
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @junior
    assert_response :success
  end

  test "should update junior" do
    patch :update, id: @junior, junior: { available: @junior.available, name: @junior.name }
    assert_redirected_to junior_path(assigns(:junior))
  end

  test "should destroy junior" do
    assert_difference('Junior.count', -1) do
      delete :destroy, id: @junior
    end

    assert_redirected_to juniors_path
  end
end
