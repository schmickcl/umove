class JuniorsController < ApplicationController
  before_action :set_junior, only: [:show, :edit, :update, :destroy]

  # GET /juniors
  # GET /juniors.json
  def index
    @juniors = Junior.all
  end

  # GET /juniors/1
  # GET /juniors/1.json
  def show
  end

  # GET /juniors/new
  def new
    @junior = Junior.new
  end

  # GET /juniors/1/edit
  def edit
  end

  # POST /juniors
  # POST /juniors.json
  def create
    @junior = Junior.new(junior_params)

    respond_to do |format|
      if @junior.save
        format.html { redirect_to @junior, notice: 'Junior was successfully created.' }
        format.json { render action: 'show', status: :created, location: @junior }
      else
        format.html { render action: 'new' }
        format.json { render json: @junior.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /juniors/1
  # PATCH/PUT /juniors/1.json
  def update
    respond_to do |format|
      if @junior.update(junior_params)
        format.html { redirect_to @junior, notice: 'Junior was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @junior.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /juniors/1
  # DELETE /juniors/1.json
  def destroy
    @junior.destroy
    respond_to do |format|
      format.html { redirect_to juniors_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_junior
      @junior = Junior.find(params[:id])
      @title = "lalaal"
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def junior_params
      params.require(:junior).permit(:name, :available)
    end
end
