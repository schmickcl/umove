json.array!(@juniors) do |junior|
  json.extract! junior, :name, :available
  json.url junior_url(junior, format: :json)
end
